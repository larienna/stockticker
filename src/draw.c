/**
    draw.c.c

    @Author: Eric Pietrocupo
    @Date: 30 sept 2022
    @License: GNU GPL 3


*/

#include <stdbool.h>
#include <rules.h>
#include <allegro.h>
#include <stdio.h>

//NOTE: Initialised in Draw_game_status
int white = -1;
int black = -1;
int stock_color [NB_STOCKS] = { -1, -1, -1, -1, -1, -1 };

void print_player_assets ( s_game *game, BITMAP *bg )
{  //TODO: OPTIMIZATION: Can glymmer be used to draw background as a text buffer, much more convenient.
   // use custom dialog with text content, problem is that variable, could be done via call backs or a custom
   // dialog. Not sure if feature currently available in glymmer
   textprintf_ex( bg, font, 0, 8, white, black, "Stock     Value | " );
   for ( int p = 0; p < NB_PLAYERS; p++ )
   {  textprintf_ex( bg, font, 144 + (64 * p), 8, white, black, "%8s ", game->player[p].name );
   }
/*
   //print stocks and player stocks
   for ( int s = 0 ; s < NB_STOCKS ; s++)
   {  printf( "\n%-10s %3.2f | ", game->market[s].name, game->market[s].value);
      for ( int p = 0;  p < NB_PLAYERS; p++ )
      {  printf ("%8d ", game->player[p].share[s] );
      }
   }

   printf ("\nMoney             ");
   for ( int p = 0;  p < NB_PLAYERS; p++ )
   {  printf ("%8d ", game->player[p].money );
   }

   printf ("\nFortune           ");
   for ( int p = 0; p < NB_PLAYERS; p++ )
   {  printf ("%8d ", player_fortune ( game, p ));
   }
*/
}

/*! @brief Draw the game baord track
*/

void draw_game_board ( s_game *game, BITMAP *bg)
{  float value = 0.05f;
   for ( int i = 0; i < 40; i++ )
   {  int y = 479 - ( (i+1)*11 ); //drawing from bottom to top
      for ( int s = 0 ; s < NB_STOCKS; s++ )
      {  int x = 639 - ( (s+1)*32 ); // drawing from right to left
         rectfill ( bg, x, y, x+30, y+9, stock_color[NB_STOCKS - s -1] );
         //printf ("%d", NB_STOCKS -s);
         float cmp = i+1;
         printf ( "DEBUG compare %f with %f\n", game->market[s].value, 0.05f * cmp );
         if ( game->market[s].value == 0.05f * cmp ) rectfill (bg, x+ 12, y+1, x+19, y+7, white );
         //TODO: BUG: does not print if value match, but print all at the right place if condition removed.
      }
      textprintf_ex (bg, font, 407, y+1, white, black, "%3.2f$", value );
      value += 0.05f;
      //printf ("\n");
   }
   //DEBUG
   /*for ( int i = 0; i < NB_STOCKS ; i++ )
   {  rectfill ( bg, 0, i*8+8, 23, i*8+16, stock_color[i] );
   }*/

}

/*! @brief Display game status in graphic mode.
   Will be the main background used to display game information
*/

void draw_game_status ( s_game *game, BITMAP *bg)
{
   if ( white == -1) white = makecol(255, 255, 255);
   if ( black == -1) black = makecol(0,0,0);
   if ( stock_color[0] == -1 )
   {  stock_color[0] = makecol ( 0, 196, 0 );
      stock_color[1] = makecol ( 136, 0, 128 );
      stock_color[2] = makecol ( 0, 144, 128);
      stock_color[3] = makecol ( 128, 96, 0);
      stock_color[4] = makecol ( 0, 0, 196);
      stock_color[5] = makecol ( 164, 164, 0);
   }


   //textprintf_centre_ex (screen, font, 320, 20, white, black , "This is a test" );

   print_player_assets( game, bg);

   draw_game_board ( game, bg);


/*
  static const char* updown[3] = { "DOWN", "DIV", "UP" };

   printf ("----------\nTurn %d\n", game->turn);
   for ( int r = 0 ; r < game->variant.nb_rolls ; r++ )
   {  printf ("%-10s %4s %d\n", game->market[game->roll[r].stockid].name,
         updown [game->roll[r].move + 1],
         game->roll[r].value );
   }

   printf ("Stock     Value | " );
   for ( int p = 0; p < NB_PLAYERS; p++ )
   {  printf ("%8s ", game->player[p].name );
   }

   //print stocks and player stocks
   for ( int s = 0 ; s < NB_STOCKS ; s++)
   {  printf( "\n%-10s %3.2f | ", game->market[s].name, game->market[s].value);
      for ( int p = 0;  p < NB_PLAYERS; p++ )
      {  printf ("%8d ", game->player[p].share[s] );
      }
   }

   printf ("\nMoney             ");
   for ( int p = 0;  p < NB_PLAYERS; p++ )
   {  printf ("%8d ", game->player[p].money );
   }

   printf ("\nFortune           ");
   for ( int p = 0; p < NB_PLAYERS; p++ )
   {  printf ("%8d ", player_fortune ( game, p ));
   }

   printf ("\n");
*/

}
