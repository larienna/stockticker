/**
    draw.h

    @Author: Eric Pietrocupo
    @Date: 30 sept 2022
    @License: GNU GPL 3

    Module that draw manually stuff on the screen like the backgrounb
*/

#ifndef DRAW_H_INCLUDED
#define DRAW_H_INCLUDED

void draw_game_status ( s_game *game, BITMAP *bg);




#endif // DRAW_H_INCLUDED
