/**
    interface.c

    @Author: Eric Pietrocupo
    @Date: 24 octobre 2022
    @License: GNU GPL 3


*/

#include <stdbool.h>
#include <rules.h>
#include <stdio.h>
#include <allegro.h>
#include <draw.h>
#include <glymmer.h>
#include <gm_common.h>

/*! @brief Display title screen and return choice
*/
int show_title_screen ( gms_glymmer *gm, BITMAP *bg )
{  gms_item title_menu [] =  { { 0, 0, "Start Game" },
                           { 1, 0, "Change Options" },
                           { -1, 0, "Quit" },
                           { 0, 0, NULL }
                         };

   //TODO: Game logo or background
   textout_centre_ex( bg, font, "Stock Ticker: Digital Edition", 320, 80, makecol(225, 225, 0), makecol (0, 64, 0));

                         /*
                         gm_dialog_open_background( &gm, "Stock Ticker" );
                         */
   //IDEA: Add auto center
   gm_dialog_open_list( gm, NULL, 18, 20, NULL, title_menu );

   int answer = gm_show_dialogs( bg, gm );
   //TODO: GLYMMER: REcurrent BUG, if pass &gm, instead of pointer gm, it will crash. See if can validate.

   return answer;

   gm_dialog_close_all( gm );

   /*gms_item choices [] =  { { 0, 0, "Choice A" },
                           { 1, 0, "Choice B" },
                           { 2, 0, "Choice C" },
                           { 0, 0, NULL }
                         };

   printf ("Stock Ticker: Test glymmer");


   gms_glymmer gm;
   gm_initialise ( &gm, fnt, 8, GM_INPUT_KEYBOARD, SCREEN_W, SCREEN_H );

   gm_dialog_open_background( &gm, "Stock Ticker" );
   gm_dialog_open_list( &gm, "List of choices", 5, 5, "Please make a choice", choices );

   int answer = gm_show_dialogs( bg, &gm );

   gm_dialog_close_all( &gm );

   gm_dispose( &gm);*/
}

int show_config_screen ( gms_glymmer *gm, BITMAP *bg )
{  gms_item divup_option_list [] =  { { false, 0, "Dividend only pays money" },
                           { true, 0, "Dividend pays money AND moves up" }
                           { 0, 0, NULL }
                         };

   gms_item nbroll_option_list [] =  { { 1, 0, "Roll once per turn" },
                           { 2, 0, "Roll twice per turn" },
                           { 3, 0, "Roll three times per turn" },
                           { 0, 0, NULL }
                         };

   //used if nb of rolls is > 1
   gms_item noduplicate_option_list [] =  { { true, 0, "Yes" },
                           { false, 0, "NO" }
                           { 0, 0, NULL }
                         };

   //TODO Ask questions in order and modify config in consequence. Maybe create a config method to encapsulate??

   gm_dialog_close_all( gm );

}


