/**
    interface.h

    @Author: Eric Pietrocupo
    @Date: 24 oct 2022
    @License: GNU GPL 3

   User interface, navigation and game menu mostly using glymmer.
*/

#ifndef INTERFACE_H_INCLUDED
#define INTERFACE_H_INCLUDED

int show_title_screen ( gms_glymmer *gm, BITMAP *bg );

int show_config_screen ( gms_glymmer *gm, BITMAP *bg );


#endif // INTERFACE_H_INCLUDED
