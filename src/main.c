/**
    St0ck T!cker

    @author: Eric Pietrocupo
    @data: 16 sept 2022
    @license: GNU GPL 3

    This is a game which is a digital implementation of the classic board game using
    allegro 4 GL.
*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <allegro.h>
#include <test.h>
#include <gm_common.h>
#include <glymmer.h>
#include <interface.h>

//--- Global Variables ---

FONT *_fnt;
BITMAP *_bg;

//--- Procedures ---

void init_gl ( void )
{  allegro_init ();

   if ( install_keyboard() != 0 )
   {  printf ("ERROR: installing keyboard\n");
      exit (1);
   }

   set_color_depth(16);
   if ( set_gfx_mode( GFX_AUTODETECT, 640, 480, 0, 0 ) != 0)
   {  printf ("ERROR: Cannot set graphic mode\n");
      exit(1);
   }

   set_projection_viewport( 0, 0, 640, 480);
   set_trans_blender ( 0, 0, 0, 255);

   //load a fon into memory
   _fnt = load_font( "WizardrySmall.bmp", NULL, NULL);
   //make sur the loading worked
   assert_or_exit( _fnt != NULL, 1, "Could not load font");

   //create a bitmap that will be used as background behind the dialogs
   _bg = create_bitmap( SCREEN_W, SCREEN_H );
   //paint the packground with a green color
   rectfill ( _bg, 0, 0, SCREEN_W, SCREEN_H, makecol(0, 0, 0 ) );

}

void stock_ticker ( void )
{
   gms_glymmer gm;
   gm_initialise ( &gm, _fnt, 8, GM_INPUT_KEYBOARD, SCREEN_W, SCREEN_H );



   int choice = -1;
   do
   {
      choice = show_title_screen ( &gm, _bg );

      //TODO: Use constants
      switch ( choice )
      {  case 0:
         case 1: show_config_screen( &gm, _bg);
      }
   }
   while (choice != -1);


   gm_dispose( &gm);

}

void dispose_gl ( void )
{  destroy_font( _fnt);
   destroy_bitmap( _bg);
   set_gfx_mode( GFX_TEXT, 80, 25, 0, 0);
   allegro_exit();
}

int main()
{  init_gl();
   //test_game_rules();
   //test_graphic_display();
   //test_glymmer( _bg, _fnt);
   stock_ticker();
   dispose_gl();
   return 0;
}
END_OF_MAIN()
