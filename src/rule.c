/**
    Rules.c

    @Author: Eric Pietrocupo
    @Date: 16 sept 2022
    @License: GNU GPL 3

   Implements most of the game's mechanics without any GUI and graphics
*/

#include <stdbool.h>
#include <stdio.h>
#include <rules.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

int roll_die ( int max )
{  double rnd = rand();
   //double rnd = RAND_MAX;
   double num = ((double)(rnd / RAND_MAX))*max;
   if ( num == max) num = max -1;
   return num;
}

void setup_game ( s_game *game, s_rule *variant, int nb_humans )
{  if ( game == NULL )
   {  printf ( "ERROR: Game structure is NULL\n" );
      exit(1);
   }

   game->turn = 0;
   //initialise players
   for ( int i = 0; i < NB_PLAYERS; i++ )
   {  game->player[i].money = 5000;
      game->player[i].name = "noname";
      game->player[i].is_human = i < nb_humans ? true: false;
      //initialise player shares
      for ( int j = 0; j < NB_STOCKS; j++)
      {  game->player[i].share[j] = 0;
      }
   }

   game->player[0].name = "Player";
   game->player[1].name = "Jimmy";
   game->player[2].name = "Jerry";
   game->player[3].name = "Johnny";

   //initialise stocks and market, useful if game reset
   for ( int i = 0; i < NB_STOCKS; i++)
   {  game->market[i].value = 1.0f;
   }

   //constant values
   game->market[0].name = "Food";
   game->market[0].code = "FOOD";
   game->market[1].name = "Industrial";
   game->market[1].code = "INDU";
   game->market[2].name = "Finance";
   game->market[2].code = "FINA";
   game->market[3].name = "Oil";
   game->market[3].code = "OIL ";
   game->market[4].name = "Technology";
   game->market[4].code = "TECH";
   game->market[5].name = "Gold";
   game->market[5].code = "GOLD";

   if ( variant == NULL ) //default rules
   {  game->variant.nb_rolls = 1;
      game->variant.divup = false;
      game->variant.noduplicate = false;
   }
   else
   {  game->variant.nb_rolls = variant->nb_rolls;
      game->variant.divup = variant->divup;
      if ( game->variant.nb_rolls > NB_MAXROLLS) game->variant.nb_rolls = NB_MAXROLLS;
   }

   //initialise randomness
   srand (time (NULL));
}

bool buy_stock ( s_game *game, int stockid, int playerid, int quantity )
{  float cost = game->market[stockid].value * quantity;

   if ( cost <= game->player[playerid].money)
   {  game->player[playerid].money -= cost;
      game->player[playerid].share[stockid] += quantity;
      return true;
   }
   else return false;

}

bool sell_stock ( s_game *game, int stockid, int playerid, int quantity )
{  float cost = game->market[stockid].value * quantity;

   if ( quantity <= game->player[playerid].share[quantity])
   {  game->player[playerid].money += cost;
      game->player[playerid].share[stockid] -= quantity;
      return true;
   }
   else return false;
}

int player_fortune ( s_game *game, int playerid)
{  float sum = game->player[playerid].money;

   for ( int i = 0 ; i < NB_STOCKS; i++)
   {  sum += game->player[playerid].share[i] * game->market[i].value;
   }
   return sum;
}


/*! @brief Select stock randomly according to rules
   Will avoid returning dupplicated if the appropriate variant is activated
*/
int roll_stock ( s_game *game )
{  int stock = roll_die(NB_STOCKS);

   if ( game->variant.noduplicate )
   {  while ( game->stock_rolled[stock] == true)
      {  printf ("DEBUG: Stock peeking %d\n", stock);
         stock++;
         stock = stock % NB_STOCKS;
      }
      game->stock_rolled[stock] = true;
   }
   printf ("DEBUG: Stock chosen %d\n", stock);
   return stock;
}

void roll_dice ( s_game *game )
{
   //reset the selected stocks
   for ( int s = 0; s < NB_STOCKS; s++)
   {  game->stock_rolled[s] = false;
   }

   for ( int r = 0 ; r < game->variant.nb_rolls; r++)
   {  game->roll[r].stockid = roll_stock( game);
      game->roll[r].value = pow ( 2, (roll_die(3))) * 5;
      game->roll[r].move = (roll_die(3)) - 1;

      //move stock
      bool divpaid = (game->roll[r].move == 0 );
      int move = game->roll[r].move;
      if ( game->variant.divup && move == 0 ) move = 1; //Variant rule where DIV also goes UP
      //if ( game->variant.divup && move == 0 ) move = -1; //DEBUG to test crash
      float increment = ( move * game->roll[r].value ) / 100.0f;

      game->market[game->roll[r].stockid].value += increment;

      //Modify player money, pay dividends

      if ( divpaid && game->market[game->roll[r].stockid].value > 1.0f)
      {  for ( int p = 0; p < NB_PLAYERS; p++ )
         {  game->player[p].money += game->player[p].share[game->roll[r].stockid] *
               ( game->roll[r].value / 100.0f );
         }
      }

      //Handle split
      if ( game->market[game->roll[r].stockid].value >= 2.0f )
      {  game->market[game->roll[r].stockid].value = 1.0f;
         for ( int p = 0; p < NB_PLAYERS; p++ )
         {  game->player[p].share[game->roll[r].stockid] *= 2;
         }
      }

      //Handle crash
      if ( game->market[game->roll[r].stockid].value <= 0.0f )
      {  game->market[game->roll[r].stockid].value = 1.0f;
         for ( int p = 0; p < NB_PLAYERS; p++ )
         {  game->player[p].share[game->roll[r].stockid] = 0;
         }
      }

   }

   game->turn++;
}

void display_game_status ( s_game *game)
{  static const char* updown[3] = { "DOWN", "DIV", "UP" };

   printf ("----------\nTurn %d\n", game->turn);
   for ( int r = 0 ; r < game->variant.nb_rolls ; r++ )
   {  printf ("%-10s %4s %d\n", game->market[game->roll[r].stockid].name,
         updown [game->roll[r].move + 1],
         game->roll[r].value );
   }

   printf ("Stock     Value | " );
   for ( int p = 0; p < NB_PLAYERS; p++ )
   {  printf ("%8s ", game->player[p].name );
   }

   //print stocks and player stocks
   for ( int s = 0 ; s < NB_STOCKS ; s++)
   {  printf( "\n%-10s %3.2f | ", game->market[s].name, game->market[s].value);
      for ( int p = 0;  p < NB_PLAYERS; p++ )
      {  printf ("%8d ", game->player[p].share[s] );
      }
   }

   printf ("\nMoney             ");
   for ( int p = 0;  p < NB_PLAYERS; p++ )
   {  printf ("%8d ", game->player[p].money );
   }

   printf ("\nFortune           ");
   for ( int p = 0; p < NB_PLAYERS; p++ )
   {  printf ("%8d ", player_fortune ( game, p ));
   }

   printf ("\n");

}
