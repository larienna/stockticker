/**
    Rules.h

    @Author: Eric Pietrocupo
    @Date: 16 sept 2022
    @License: GNU GPL 3
*/

#ifndef RULES_H_INCLUDED
#define RULES_H_INCLUDED

//-------------------------------------- Constants ----------------------------------------

#define NB_STOCKS 6
#define NB_PLAYERS 4
#define NB_MAXROLLS 3

//stock id

#define FOOD         0
#define INDUSTRIAL   1
#define FINANCE      2
#define OIL          3
#define TECHNOLOGY   4
#define GOLD         5

//---------------------------------------- Data Structures ---------------------------------

/*! @brief Player information
   Contains the detailed information relative to each player
*/
typedef struct s_player
{  char *name;
   int money;
   int share [NB_STOCKS];
   bool is_human;
}s_player;

/*! @brief Market values
   Contains the various values for each of the markets
*/

typedef struct s_market
{  char *name;
   char *code;
   float value;
}s_market;

/*! @brief Variant rules
   Contains game variant rule options
*/

typedef struct s_rule
{  int nb_rolls; //! Number of rolls per turn, default is 1
   bool divup; //! Dividend rolls also goes up
   bool noduplicate; //! Avoid rolling the same stock multiple time if more than 1 roll is used.
                     //! Basically, it makes the stock die become cards
}s_rule;

/*! @brief Roll results
   Store the results of a roll for various usage
*/

typedef struct s_roll
{  int stockid;
   int value; //encode with real value 5 10 20
   int move; //! Encoded as +1 UP, -1 DOWN, 0 DIV
}s_roll;

/*! @brief Game information
   Contains all th information about the status of the game. THis is the structure that
   should be stored into a savegame
*/
//TODO: See if can use an XML parser, or JSON parser to save the game data to file since no relational database.
typedef struct s_game
{  s_player player[NB_PLAYERS];
   s_market market[NB_STOCKS];
   /*s_market market[NB_STOCKS] = { { .name = "Food",       .code = "FOOD", .value=1.0f },
                                  { .name = "Industrial", .code = "INDU", .value=1.0f },
                                 { .name = "Bonds",      .code = "BOND", .value=1.0f },
                                  { .name = "Oil",        .code = "OIL ", .value=1.0f },
                                  { .name = "Technology", .code = "TECH", .value=1.0f },
                                  { .name = "Gold",       .code = "GOLD", .value=1.0f }
                                };*/
   s_rule variant;
   s_roll roll [NB_MAXROLLS]; //! Remember the last rolls made
   bool stock_rolled [NB_STOCKS]; //!Remember which stock was rolled to avoid duplicates.
   int turn; //Id of the current turn starting from 0 indicating the setup.
}s_game;

//--------------------------------------- Prototypes -----------------------------------------

int roll_die ( int max );

void setup_game ( s_game *game, s_rule *variant, int nb_humans );

bool buy_stock ( s_game *game, int stockid, int playerid, int quantity );

bool sell_stock ( s_game *game, int stockid, int playerid, int quantity );

int player_fortune ( s_game *game, int playerid);

void roll_dice ( s_game *game );

void display_game_status ( s_game *game);

#endif // RULES_H_INCLUDED
