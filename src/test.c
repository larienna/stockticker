/**
    test.c

    @Author: Eric Pietrocupo
    @Date: 23 sept 2022
    @License: GNU GPL 3

   functional tests
*/

#include <stdbool.h>
#include <rules.h>
#include <stdio.h>
#include <allegro.h>
#include <draw.h>
#include <glymmer.h>
#include <gm_common.h>

void buy_test_stocks ( s_game *game )
{  buy_stock( game, 1, 0, 1000 );
   buy_stock( game, 2, 0, 1000 );
   buy_stock( game, 3, 0, 1000 );
   buy_stock( game, 4, 0, 1000 );
   buy_stock( game, 5, 0, 1000 );

   buy_stock( game, 0, 1, 1000 );
   buy_stock( game, 1, 1, 1000 );
   buy_stock( game, 2, 1, 1000 );
   buy_stock( game, 3, 1, 1000 );
   buy_stock( game, 4, 1, 1000 );

   buy_stock( game, 1, 2, 1000 );
   buy_stock( game, 1, 2, 1000 );
   buy_stock( game, 3, 2, 1000 );
   buy_stock( game, 3, 2, 1000 );
   buy_stock( game, 5, 2, 1000 );

   buy_stock( game, 0, 3, 1000 );
   buy_stock( game, 2, 3, 1000 );
   buy_stock( game, 2, 3, 1000 );
   buy_stock( game, 4, 3, 1000 );
   buy_stock( game, 4, 3, 1000 );

}

void test_game_rules ( void )
{
   printf("Stock Ticker demonstration\n");
   s_game game;
   s_rule variant = { .nb_rolls = 3, .divup = false, .noduplicate = true };


   setup_game ( &game, &variant, 1 );

   buy_test_stocks( &game);

   int input;
   for ( int i = 0 ; i < 10 ; i++ )
   //while (true)
   {  roll_dice( &game );
      display_game_status( &game);
      //scanf ("%d ", &input );
   }
}

/*! @brief Test graphic game status
   Requires the graphic mode to be previously setup
*/

void test_graphic_display ( void )
{


   printf("Stock Ticker graphic demonstration\n");
   s_game game;
   s_rule variant = { .nb_rolls = 3, .divup = false, .noduplicate = true };

   setup_game ( &game, &variant, 1 );

   buy_test_stocks( &game);

   draw_game_status( &game ,screen);
   unsigned char key;
   do
   {  key = readkey() >> 8;
      //textprintf_centre_ex (screen, font, 320, 30, makecol(255, 255, 255), makecol(0,0,0),
      //"KEY PRESSED is %d", key );
   }
   while ( key != KEY_ENTER );

//   for ( int i = 0 ; i < 10 ; i++ )
   //while (true)
//   {  roll_dice( &game );



      //scanf ("%d ", &input );
//   }
}



void test_glymmer ( BITMAP *bg, FONT *fnt )
{  gms_item choices [] =  { { 0, 0, "Choice A" },
                           { 1, 0, "Choice B" },
                           { 2, 0, "Choice C" },
                           { 0, 0, NULL }
                         };

   printf ("Stock Ticker: Test glymmer");


   gms_glymmer gm;
   gm_initialise ( &gm, fnt, 8, GM_INPUT_KEYBOARD, SCREEN_W, SCREEN_H );

   gm_dialog_open_background( &gm, "Stock Ticker" );
   gm_dialog_open_list( &gm, "List of choices", 5, 5, "Please make a choice", choices );

   int answer = gm_show_dialogs( bg, &gm );

   gm_dialog_close_all( &gm );

   gm_dispose( &gm);

}
