/**
    test.h

    @Author: Eric Pietrocupo
    @Date: 23 sept 2022
    @License: GNU GPL 3

   functional tests
*/

#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED

void test_game_rules ( void );
void test_graphic_display ( void );
void test_glymmer ( BITMAP *bg, FONT *fnt );

#endif // TEST_H_INCLUDED
